// This is where it all goes :)
var Komplott = Komplott || {};
Komplott.slideShow = {
    show: function(imageNumber) {
        $('.slideshow-item.active').toggleClass('active');
        var imageItem = $('.slideshow-item').filter(function() {return $(this).data('image-number') === imageNumber});
        imageItem.toggleClass('active');
        $('#imageModal').modal('show');
    }
};

$(function () {
    $.fn.matchHeight._maintainScroll = true;
    $('.js-adjust-height').matchHeight({
        byRow: true,
        property: 'height',
        target: null,
        remove: false
    });
    $('#accordion').on('shown.bs.collapse', function () {
        $.fn.matchHeight._update();
    });
});

$(function() {
    $('.image-frame').on('click', function() {
        var imageNumber = $(this).data('image-number');
        Komplott.slideShow.show(imageNumber);
    });
});